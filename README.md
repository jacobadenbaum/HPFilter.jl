# HP Filtering

This module provides simple code for fast and efficient HP filtering.
It solves the HP system using sparse methods for any arbitrary time
series.

A typical example of HPFilter.jl would be to pull some aggregate time
series data (such as the unemployment rate) and filter the data.  

```julia
using HPFilter
using FredData

f = Fred()
df = get_data(f, "UNRATE").data

# Montly
df = df[[:date, :value]]
df[:trend] = hpfilter(df[:value], 1600*3^4)
df[:cycle] = df[:value] - df[:trend]
```

After we filter the data, we can plot it to visualize the inferred
cyclical components and the trends

```julia
using PyPlot
fig, ax = subplots() 
plot(df[:date], df[:value], label="Civilian Unemployment")
plot(df[:date], df[:trend], label="HP Filter Trend")
plot(df[:date], df[:cycle], label="HP Filter Cycle")
ax[:set_title]("US Civilian Unemployment Rate")
legend(fontsize="x-small")
```
If you're following along, you should get the following plot:
![US Unemployment](readme_images/us_unemployment.jpg)
