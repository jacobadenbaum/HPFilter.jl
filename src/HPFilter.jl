module HPFilter

using BandedMatrices
export hpfilter

########################################################################
#################### HP Filtering ######################################
########################################################################

function hpfilter(y::Vector{R}, λ::S=1600) where {R<:Number, S<:Real}
    
    # Construct Linear System of First Order Conditions
    T = length(y)
    A = BandedMatrix(Zeros{promote_type(S, Int)}(T,T), (2, 2))
    b = zeros(R, T)
    for t=1:T
        # Generic Case
        if 2 < t < T-1
            A[t, t-2] = 1λ
            A[t, t-1] = -4λ
            A[t, t]   =  6λ + 1
            A[t, t+1] = -4λ
            A[t, t+2] = 1λ
            b[t]      = y[t]
        # Corner Cases
        elseif t == 1
            A[t, t]   = 1λ + 1
            A[t, t+1] = -2λ
            A[t, t+2] = 1λ
            b[t]      = y[t]
        elseif t == 2
            A[t, t-1] = -2λ
            A[t, t]   = 5λ + 1
            A[t, t+1] = -4λ
            A[t, t+2] = 1λ
            b[t]      = y[t]
        elseif t == T-1
            A[t, t-2] = 1λ
            A[t, t-1] = -4λ
            A[t, t]   = 5λ + 1
            A[t, t+1] = -2λ

            b[t]      = y[t]
        elseif t == T
            A[t, t-2] = 1λ
            A[t, t-1] = -2λ
            A[t, t]   = 1λ + 1
            b[t]      = y[t]
        end
    end
    
    # Solve the system
    g = A\b

    # Compute drop the boundary conditions
    return g
end

end
